# Repository Description

This repository hosts the verifier archives for
the International Competition on Software Verification (SV-COMP).

# Contributing

Participants of SV-COMP can file a pull request to this repository in order
to make their verifier available for the competition execution,
to the SV-COMP jury for inspection, and for later replication of the results.

For SV-COMP 2019, the archives are stored as
`2019/<verifier>.zip`, where `<verifier>` is
the identifier for the verifier,
i.e., there exists a file `<verifier>.xml`
in the repository with the benchmark definitions:
https://github.com/sosy-lab/sv-comp/tree/master/benchmark-defs

Validators use a prefix `val_`, and can be sym-linked to a verifier archive.

By filing a pull/merge request to this repository, contributors confirm
that the license for the archive is compatible with
the requirements of SV-COMP, as outlined in the rules
https://sv-comp.sosy-lab.org/2019/rules_2018-10-03.pdf
under section Competition Environment and Requirements / Verifier.

# Reduction of Repository Size:

Note (dbeyer 2018-11-19):
To reduce the size of the repository (we were at 11.8 GB, while the (soft) quota is 10 GB),
I removed the largest three archives from the repository, using the following commands,
and ask the three teams to resubmit the latest version:

`git filter-branch --index-filter 'git rm --cached --ignore-unmatch 2019/skink.zip 2019/veriabs.zip 2019/map2check.zip' HEAD`

`git push --force`

